#!/usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive

sudo apt update
sudo apt upgrade -y

sudo apt-get install -y php php-gd php-xml php-bcmath php-bz2 php-intl php-mbstring php-mysql php-zip php-curl php-memcached unzip apache2 libapache2-mod-php memcached chromium-browser chromium-chromedriver


cd /vagrant/
curl https://getcomposer.org/installer -o composer-setup.php
php composer-setup.php
rm  composer-setup.php
/vagrant/composer.phar install
mv composer.phar /usr/local/bin/composer
