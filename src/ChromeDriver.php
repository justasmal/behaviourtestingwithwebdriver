<?php

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Chrome\ChromeDriverService;
use Facebook\WebDriver\Chrome\ChromeOptions;

/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-28
 * Time: 23:12
 */



class ChromeDriver
{
    private $chromeBrowserService;
    private $chromeBrowserDriver;

    private function getFreePort()
    {
        $sock = socket_create_listen(0);
        socket_getsockname($sock, $address, $port);
        socket_close($sock);
        return $port;
    }

    public function startChromeBrowser($extraChromeCommands = array(), $extraDriverCommands = array())
    {
        $executableDriver = "/usr/lib/chromium-browser/chromedriver";
        $executableChrome = "/usr/lib/chromium-browser/chromium-browser";
        $driverPort = $this->getFreePort();
        $driverArgs = array(
            "--port=$driverPort",
            "--log-path=/tmp/webdriver_log.".$driverPort."." . time() . ".txt "
            //,'--log-level=DEBUG'
        );
        if(isset($extraDriverCommands))
        {
            $driverArgs = array_merge($driverArgs, $extraDriverCommands);
        }
        $environment = null;
        $this->chromeBrowserService = new ChromeDriverService($executableDriver, $driverPort, $driverArgs, $environment);
        $this->chromeBrowserService->start();
        $host = "http://127.0.0.1:$driverPort";
        $chromePort = $this->getFreePort();
        $options = new ChromeOptions();
        $chromeArgs = array(
            "--headless",
            "--no-sandbox",
            "--window-size=1920,1080",
            "--start-maximized",
            "--disable-dev-shm-usage",
            "--disable-accelerated-2d-canvas",
            "--single-process",
            "--ignore-certificate-errors",
            "--lang=en",
            "--remote-debugging-port=$chromePort"
        );
        if(isset($extraChromeCommands))
        {
            $chromeArgs = array_merge($chromeArgs, $extraChromeCommands);
        }
        $options->setBinary($executableChrome);
        $options->addArguments($chromeArgs);
        $capabilities = DesiredCapabilities::chrome();
        $capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
        $capabilities->setCapability("acceptSslCerts", true);
        $capabilities->setCapability("acceptInsecureCerts", true);
        $this->chromeBrowserDriver = RemoteWebDriver::create($host, $capabilities, 5000);
        return $this->chromeBrowserDriver;
    }

    /**
     *  Closes Chrome and ChromeWebDriver services if any is running
     */
    public function closeChromeBrowser()
    {
        if ($this->chromeBrowserDriver) {
            $this->chromeBrowserDriver->close();
        }
        if ($this->chromeBrowserDriver) {
            $this->chromeBrowserDriver->quit();
        }

        if ($this->chromeBrowserService && $this->chromeBrowserService->isRunning()) {
            $this->chromeBrowserService->stop();
        }
    }
}