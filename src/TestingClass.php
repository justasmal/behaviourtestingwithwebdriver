<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-01-28
 * Time: 23:07
 */

use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;

require_once("ChromeDriver.php");


class TestingClass extends PHPUnit\Framework\TestCase
{
    protected $driver;
    protected $chromeDrv;
    private function startup()
    {
        $this->chromeDrv = new ChromeDriver();
        $this->driver = $this->chromeDrv->startChromeBrowser();
    }
    private function shutdown()
    {
        $this->driver = null;
        $this->chromeDrv->closeChromeBrowser();
        $this->chromeDrv = null;
    }
    private function getPath($methodname)
    {
        $path = dirname(__DIR__, 1) . "/screenshot/screenshot" . $methodname . "." . time() . ".png";
        return $path;
    }

    /** @test */
    public function openPageTest()
    {
        $this->startup();
        $this->driver->get("https://google.lt/");
        $this->driver->takeScreenshot($this->getPath(__FUNCTION__));
        $title = $this->driver->getTitle();
        $this->shutdown();
        $this->assertEquals("Google", $title);
    }
    /** @test */
    public function checkUsernameInputCount()
    {
        $path = $this->getPath(__FUNCTION__);
        $usernameinputcount = (object)array();
        try
        {
            $this->startup();
            $this->driver->get("https://moodle.vgtu.lt/");
            $this->driver->wait(10, 500)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id("username"))
            );
            $usernameinputcount = $this->driver->findElements(WebDriverBy::id("username"));
            $this->shutdown();
        }
        catch (Exception $e)
        {
            $this->driver->takeScreenshot($path);
            $this->shutdown();
            $this->assertEquals(1, count($usernameinputcount));
        }
        $this->assertEquals(1, count($usernameinputcount));

    }

    /** @test */
    public function insertBadUserInput()
    {
        $path = $this->getPath(__FUNCTION__);
        $errorVisible = false;
        try
        {
            $this->startup();
            $this->driver->get("https://moodle.vgtu.lt/");
            $this->driver->wait(10, 500)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id("username"))
            );
            $this->driver->wait(10, 500)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector(".password > input"))
            );
            $this->driver->findElement(WebDriverBy::id("username"))->sendKeys("20153248a");
            $this->driver->findElement(WebDriverBy::cssSelector(".password > input"))->sendKeys("testas123");
            $this->driver->takeScreenshot($this->getPath(__FUNCTION__));
            $this->driver->findElement(WebDriverBy::cssSelector('.buttons > input'))->click();
            $this->driver->wait(10, 500)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className("error_text"))
            );
            $text = $this->driver->findElement(WebDriverBy::className("error_text"))->getText();
            if(strpos($text, "Klaida") >= 0)
            {
                $errorVisible = true;
            }
            $this->driver->takeScreenshot($this->getPath(__FUNCTION__));
            $this->shutdown();
        }
        catch (Exception $e)
        {
            $this->driver->takeScreenshot($path);
            $this->shutdown();
            $this->assertTrue($errorVisible);
        }

        $this->assertTrue($errorVisible);

    }

    /** @test */
    public function duckduckGoSearch()
    {
        $path = $this->getPath(__FUNCTION__);
        $min10results = false;
        try
        {
            $this->startup();
            $this->driver->get("https://duckduckgo.com/");
            $this->driver->wait(10, 500)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector("#search_form_input_homepage"))
            );
            $this->driver->findElement(WebDriverBy::cssSelector("#search_form_input_homepage"))->sendKeys("VGTU");
            $this->driver->findElement(WebDriverBy::cssSelector('#search_button_homepage'))->click();
            $this->driver->wait(10, 500)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className("results--main"))
            );
            $countResults = $this->driver->findElements(WebDriverBy::cssSelector("#links > .results_links_deep"));
            if(count($countResults) == 10)
            {
                $min10results = true;
            }
            $this->driver->takeScreenshot($this->getPath(__FUNCTION__));
            $this->shutdown();
        }
        catch (Exception $e)
        {
            $this->driver->takeScreenshot($path);
            $this->shutdown();
            $this->assertTrue($min10results);
        }
        $this->assertTrue($min10results);

    }

}